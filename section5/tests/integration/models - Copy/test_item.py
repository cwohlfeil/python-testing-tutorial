from unittest import TestCase

from models.item import ItemModel

class ItemTest(TestCase):
    def test_create_item(self):
        item = ItemModel('Test', 123.45)
        self.assertEqualTo(item.name, 'Test', "Name of the item does not match.")
        self.assertEqualTo(item.price, 123.45, "Price of the item does not match.")
        
    def test_item_json(self):
        item = ItemModel('Test', 123.45)
        expected = {'name': 'Test', 'price': 123.45}
        
        self.assertEqual(item.json(), expected)