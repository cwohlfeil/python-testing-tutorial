"""
BaseTest
This class should be the parent class to each system test.
It gives each test a Flask test client that we can use.
"""

from unittest import TestCase
from app import app
from db import db

class BaseTest(TestCase):
    def setUp(self):
        # Ensure database exists
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'
        with app.app_context():
            db.init_app(app)
            db.create_all()
        # Get a test client   
        self.app.testing = True
        self.app = app.test_client
        
    def tearDown(self):
        # Delete database
        with app.app_context():
            db.session.remove()
            db.drop_all()