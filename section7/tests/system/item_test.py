from models.user import UserModel
from models.item import ItemModel
from models.store import StoreModel
from tests.base_test import BaseTest
import json


class ItemTest(BaseTest):
    """Item systems test."""
    
    def setUp(self):
        """Setup to handle requirements for all tests."""
        super(ItemTest, self).setUp()
        with self.app() as c:
            with self.app_context():
                UserModel('test', '1234').save_to_db()
                auth_request = c.post('/auth', data=json.dumps({
                    'username': 'test',
                    'password': '1234'
                }), headers={'Content-Type': 'application/json'})
                self.auth_header = f"JWT {json.loads(auth_request.data)['access_token']}"

                
    def test_item_no_auth(self):
        """Test 401 is returned if user is no authenticated."""
        with self.app() as c:
            r = c.get('/item/test')
            self.assertEqual(r.status_code, 401)

            
    def test_item_not_found(self):
        """Test 404 is returned if item is not found."""
        with self.app() as c:
            r = c.get('/item/test', headers={'Authorization': self.auth_header})
            self.assertEqual(r.status_code, 404)

            
    def test_item_found(self):
        """Test item is returned."""
        with self.app() as c:
            with self.app_context():
                StoreModel('test').save_to_db()
                ItemModel('test', 17.99, 1).save_to_db()
                r = c.get('/item/test', headers={'Authorization': self.auth_header})

                self.assertEqual(r.status_code, 200)
                self.assertDictEqual(d1={'name': 'test', 'price': 17.99},
                                     d2=json.loads(r.data))

                
    def test_delete_item(self):
        """Test item is deleted."""
        with self.app() as c:
            with self.app_context():
                StoreModel('test').save_to_db()
                ItemModel('test', 17.99, 1).save_to_db()
                r = c.delete('/item/test')

                self.assertEqual(r.status_code, 200)
                self.assertDictEqual(d1={'message': 'Item deleted'},
                                     d2=json.loads(r.data))

                
    def test_create_item(self):
        """Test item is created."""
        with self.app() as c:
            with self.app_context():
                StoreModel('test').save_to_db()
                r = c.post('/item/test', data={'price': 17.99, 'store_id': 1})

                self.assertEqual(r.status_code, 201)
                self.assertEqual(ItemModel.find_by_name('test').price, 17.99)
                self.assertDictEqual(d1={'name': 'test', 'price': 17.99},
                                     d2=json.loads(r.data))

                
    def test_create_duplicate_item(self):
        """Test 400 is returned if existing item is created."""
        with self.app() as c:
            with self.app_context():
                StoreModel('test').save_to_db()
                c.post('/item/test', data={'price': 17.99, 'store_id': 1})
                r = c.post('/item/test', data={'price': 17.99, 'store_id': 1})

                self.assertEqual(r.status_code, 400)

                
    def test_put_item(self):
        """Test 200 is returned if item is created via put."""
        with self.app() as c:
            with self.app_context():
                StoreModel('test').save_to_db()
                r = c.put('/item/test', data={'price': 17.99, 'store_id': 1})

                self.assertEqual(r.status_code, 200)
                self.assertEqual(ItemModel.find_by_name('test').price, 17.99)
                self.assertDictEqual(d1={'name': 'test', 'price': 17.99},
                                     d2=json.loads(r.data))

                
    def test_put_update_item(self):
        """Test 200 is returned if item is updated via put."""
        with self.app() as c:
            with self.app_context():
                StoreModel('test').save_to_db()
                c.put('/item/test', data={'price': 17.99, 'store_id': 1})
                r = c.put('/item/test', data={'price': 18.99, 'store_id': 1})

                self.assertEqual(r.status_code, 200)
                self.assertEqual(ItemModel.find_by_name('test').price, 18.99)

                
    def test_item_list(self):
        """Test item list is returned."""
        with self.app() as c:
            with self.app_context():
                StoreModel('test').save_to_db()
                ItemModel('test', 17.99, 1).save_to_db()
                r = c.get('/items')

                self.assertDictEqual(d1={'items': [{'name': 'test', 'price': 17.99}]},
                                     d2=json.loads(r.data))
