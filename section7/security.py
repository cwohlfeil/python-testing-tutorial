from werkzeug.security import safe_str_cmp # backwards compatible string comparison
from models.user import UserModel


def authenticate(username, password):
    """
    Called when a user requests the /auth endpoint with their username and password.
    """
    user = UserModel.find_by_username(username)
    if user and safe_str_cmp(user.password, password):
        return user


def identity(payload):
    """
    Called when a user has already authenticated and JWT verifies them.
    """
    user_id = payload['identity']
    return UserModel.find_by_id(user_id)
