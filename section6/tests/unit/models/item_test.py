from tests.base_test import BaseTest
from models.item import ItemModel
from models.store import StoreModel # unused import for foreign key constraints


class ItemTest(BaseTest):
    def test_create_item(self):
        item = ItemModel('test', 19.99)
        self.assertEqual(item.name, 'test',
                         "The name of the item after creation does not equal the constructor argument.")
        self.assertEqual(item.price, 19.99,
                         "The price of the item after creation does not equal the constructor argument.")

    def test_item_json(self):
        item = ItemModel('test', 19.99)
        expected = { 'name': 'test', 'price': 19.99 }
        self.assertEqual(item.json(), expected,
            f"The JSON export of the item is incorrect. Received {item.json()}, expected {expected}.")
