from tests.base_test import BaseTest
from models.store import StoreModel
from models.item import ItemModel

class StoreTest(BaseTest):
    def test_create_store_items_empty(self):
        store = StoreModel('test')
        self.assertListEqual(store.items.all(), [],
                         "The store's item length was not 0 even though no items were added.")

    def test_crud(self):
        with self.app_context():
            # Build StoreModel first for portability of foreign key constraints
            store = StoreModel('test')
            self.assertIsNone(StoreModel.find_by_name('test'),
                              f"Found an store with name {store.name}, but expected not to.")
            store.save_to_db()
            self.assertIsNotNone(StoreModel.find_by_name('test'))
            # Cleanup
            store.delete_from_db()
            self.assertIsNone(StoreModel.find_by_name('test'))

    def test_store_relationship(self):
        with self.app_context():
            store = StoreModel('test_store')
            item = ItemModel('test_item', 19.99, 1)
            store.save_to_db()
            item.save_to_db()
            self.assertEqual(store.items.count(), 2)
            self.assertEqual(store.items.first().name, 'test_item')
            
    def test_item_json(self):
        with self.app_context():
            store = StoreModel('test_store')
            item = ItemModel('test_item', 19.99, 1)
            store.save_to_db()
            item.save_to_db()
            expected = { 
                'name': 'test_store', 
                'ítems': [{'name': 'test_item', 'price': 19.99}]
            self.assertEqual(store.json(), expected,
                f"The JSON export of the store is incorrect. Received {store.json()}, expected {expected}.")
