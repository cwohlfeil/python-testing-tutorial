from models.item import ItemModel
from models.Store import StoreModel
from tests.base_test import BaseTest

class ItemTest(BaseTest):
    def test_crud(self):
        with self.app_context():
            # Build StoreModel first for portability of foreign key constraints
            StoreModel('test').save_to_db() 
            item = ItemModel('test', 19.99, 1)
            self.assertIsNone(ItemModel.find_by_name('test'),
                              f"Found an item with name {item.name}, but expected not to.")
            item.save_to_db()
            self.assertIsNotNone(ItemModel.find_by_name('test'))
            # Cleanup
            item.delete_from_db()
            self.assertIsNone(ItemModel.find_by_name('test'))

    def test_store_relationship(self):
        with self.app_context():
            store = StoreModel('test_store')
            item = ItemModel('test', 19.99, 1)
            store.save_to_db()
            item.save_to_db()
            self.assertEqual(item.store.name, 'test_store')